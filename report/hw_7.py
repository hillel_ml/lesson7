from operator import index

from sklearn.model_selection import train_test_split

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder

from sklearn.neighbors import NearestCentroid

from sklearn.metrics import classification_report

from lazypredict.Supervised import LazyRegressor
from lazypredict.Supervised import LazyClassifier

import xgboost
import shap

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import time
import umap
import hdbscan




stroke_ds = pd.read_csv('lesson6/stroke_data.csv')

# print(stroke_ds.head())

# ====== Encoding ======

# Removing NaN values
stroke_ds = stroke_ds.dropna()

# Printing the target data column
print("Printing the target data column:")
print(stroke_ds.stroke)

# We count the number of zeros and ones in the target data column and print it
print("We count the number of zeros and ones in the target data column and print it")
print(stroke_ds.stroke.value_counts())


# Removing the "id" column
stroke_ds = stroke_ds.drop(columns=["id"])


# Assigning Binary Values to Two-Value Qualitative Characteristics
le = LabelEncoder()
stroke_ds["gender"] = le.fit_transform(stroke_ds["gender"])
stroke_ds["ever_married"] = le.fit_transform(stroke_ds["ever_married"])
stroke_ds["residence_type"] = le.fit_transform(stroke_ds["residence_type"])

# print(stroke_ds.head())


# One-hot encoding - Assigning a table of binary values for qualitative characteristics with multiple values
stroke_ds = pd.get_dummies(stroke_ds, columns = ['work_type', 'smoking_status'])

# Move the "stroke" column to the end of the table
stroke_ds.insert(17, 'stroke', stroke_ds.pop('stroke'))

# Renaming some columns
stroke_ds = stroke_ds.rename(columns={
    "work_type_Self-employed": "work_type_Self",
    "smoking_status_formerly smoked": "smoking_status_formerly",
    "smoking_status_never smoked": "smoking_status_never",
    "smoking_status_Unknown": "smoking_status_unknown"
    })

# print(stroke_ds.head())


# ====== first lazy predict ======


target = "stroke"
x_target = stroke_ds.loc[:, stroke_ds.columns != target]
y_target = stroke_ds.loc[:, stroke_ds.columns == target]

X_train, X_test, y_train, y_test = train_test_split(x_target, y_target, test_size=0.20, random_state=42)

# for regression can be --->
# reg = LazyClassifier(verbose=0,ignore_warnings=False, custom_metric=None)

clf = LazyClassifier(verbose=0,ignore_warnings=True, custom_metric=None)

models, predictions = clf.fit(X_train, X_test, y_train, y_test)

print("results before shap:")
print(models)


# ====== first SHAP ======

shap.initjs() # you need this so the plots can be displayed


model_XGBRegressor = xgboost.XGBRegressor().fit(x_target, y_target)


# explain the model's predictions using SHAP
# (same syntax works for LightGBM, CatBoost, scikit-learn, transformers, Spark, etc.)
explainer = shap.Explainer(model_XGBRegressor)
shap_values = explainer(x_target)


# visualize the prediction's explanation
shap.plots.waterfall(shap_values[0], show=True)
# plt.savefig('lesson7/figures/waterfall_bf_umap.png')

shap.plots.beeswarm(shap_values, show=True)
# plt.savefig('lesson7/figures/beeswarm_bf_umap.png')

shap.plots.bar(shap_values, show=True)
# plt.savefig('lesson7/figures/bar_bf_umap.png')

shap.plots.scatter(shap_values[:,"gender"], color=shap_values, show=True)
# plt.savefig('lesson7/figures/scatter_gender_bf_umap.png')

shap.plots.scatter(shap_values[:,"age"], color=shap_values, show=True)
# plt.savefig('lesson7/figures/scatter_age_bf_umap.png')

shap.plots.scatter(shap_values[:,"hypertension"], color=shap_values, show=True)
# plt.savefig('lesson7/figures/scatter_hypertension_bf_umap.png')

shap.plots.scatter(shap_values[:,"heart_disease"], color=shap_values, show=True)
# plt.savefig('lesson7/figures/scatter_heart_disease_bf_umap.png')

shap.plots.scatter(shap_values[:,"avg_glucose_level"], color=shap_values, show=True)
# plt.savefig('lesson7/figures/scatter_glucose_bf_umap.png')

shap.plots.scatter(shap_values[:,"bmi"], color=shap_values, show=True)
# plt.savefig('lesson7/figures/scatter_bmi_bf_umap.png')


# Removing columns
stroke_after_rm = stroke_ds.drop(columns=["gender",
                                    "ever_married",
                                    "work_type_Govt_job",
                                    "work_type_Never_worked",
                                    "work_type_Private",
                                    "work_type_Self",
                                    "work_type_children",
                                    "smoking_status_never"
                                    ])


# Removing NaN values
stroke_after_rm = stroke_after_rm.dropna()


# print(stroke_after_rm.head())
# print(list(stroke_after_rm))




# ====== second lazy predict ======

# target = "stroke"
x_target = stroke_after_rm.loc[:, stroke_after_rm.columns != target]
y_target = stroke_after_rm.loc[:, stroke_after_rm.columns == target]

X_train, X_test, y_train, y_test = train_test_split(x_target, y_target, test_size=0.20, random_state=42)

# for regression can be --->
# reg = LazyClassifier(verbose=0,ignore_warnings=False, custom_metric=None)

clf = LazyClassifier(verbose=0,ignore_warnings=True, custom_metric=None)

models, predictions = clf.fit(X_train, X_test, y_train, y_test)

print("results after shap:")
print(models)


# ====== Понижение размерности иходного набора данных ======

# We build a matrix of scatterplots with paired functions in order to get an idea of the data structure
# sns.set(style='darkgrid', context='paper', rc={'figure.figsize':(14,10)})

# g = sns.PairGrid(stroke_ds, hue="stroke")
# g.map_diag(sns.histplot)
# g.map_diag(sns.histplot, multiple="stack", element="step")
# g.map_offdiag(sns.scatterplot)
# g.add_legend()
# g.savefig('lesson7/figures/stroke.png')
# plt.close()


# Constructing a UMAP object and setting parametres
reducer = umap.UMAP(
    n_neighbors=15, # default value is 15
    min_dist=0.0, # default value is 0.1
    # n_components=2, # default value is 2
    metric='mahalanobis'
)

# Converting each feature into z-scores (number of standard deviations from the mean) for comparability
stroke_data = stroke_ds[
    [
        "gender",
        "age",
        "hypertension",
        "heart_disease",
        "ever_married",
        "residence_type",
        "avg_glucose_level",
        "bmi",
        "work_type_Govt_job",
        "work_type_Never_worked",
        "work_type_Private",
        "work_type_Self",
        "work_type_children",
        "smoking_status_unknown",
        "smoking_status_formerly",
        "smoking_status_never",
        "smoking_status_smokes"
    ]
].values


scaled_stroke_data = StandardScaler().fit_transform(stroke_data)


# For reducing representation of the data we will use, the fit_transform method
# which first calls fit and then returns the transformed data as a numpy array
embedding = reducer.fit_transform(scaled_stroke_data)
print(embedding.shape)

# print("embedding:")
# print(embedding)

# print("scaled_stroke_data:")
# print(scaled_stroke_data)


# Visualization of results
# plt.figure()
# plt.scatter(
#     embedding[:, 0],
#     embedding[:, 1],
#     c=[sns.color_palette()[x] for x in stroke_after_rm.stroke])
# plt.gca().set_aspect('equal', 'datalim')
# plt.title('UMAP projection of the stroke dataset', fontsize=24)
# plt.savefig('lesson7/figures/stroke_embeddings.png')


# # Appling hdbscan and set parameters
# hdbscan_labels = hdbscan.HDBSCAN(
#     min_samples=10,
#     min_cluster_size=500
# ).fit_predict(embedding)


# # Visualization of results
# plt.figure()
# plt.scatter(embedding[:, 0],
#             embedding[:, 1],
#             c=hdbscan_labels,
#             )
# plt.gca().set_aspect('equal', 'datalim')
# plt.title('HDBSCAN projection of the stroke dataset', fontsize=24)
# plt.savefig('lesson7/figures/stroke_hdbscan.png')



embedding_df = pd.DataFrame(embedding, columns = ['x_target','y_target'])
# hdbscan_df = pd.DataFrame(hdbscan_labels, columns = ['target_x'])

# embedding_df = embedding_df.dtypes

embedding_df["stroke"] = stroke_after_rm.stroke

# Move the "stroke" column to the end of the table
embedding_df.insert(2, 'stroke', embedding_df.pop('stroke'))

# Removing NaN values
embedding_df = embedding_df.dropna()

# print(embedding_df.head(10))
# print(hdbscan_df.head())



# ====== third lazy predict ======

# target = "stroke"
x_target = embedding_df.loc[:, embedding_df.columns != target]
y_target = embedding_df.loc[:, embedding_df.columns == target]

X_train, X_test, y_train, y_test = train_test_split(x_target, y_target, test_size=0.20, random_state=42)

# for regression can be --->
# reg = LazyClassifier(verbose=0,ignore_warnings=False, custom_metric=None)

clf = LazyClassifier(verbose=0,ignore_warnings=True, custom_metric=None)

models, predictions = clf.fit(X_train, X_test, y_train, y_test)

print("results after umap:")
print(models)

# ====== second SHAP ======

model_XGBRegressor = xgboost.XGBRegressor().fit(x_target, y_target)


# explain the model's predictions using SHAP
# (same syntax works for LightGBM, CatBoost, scikit-learn, transformers, Spark, etc.)
explainer = shap.Explainer(model_XGBRegressor)
shap_values = explainer(x_target)


# visualize the prediction's explanation
shap.plots.waterfall(shap_values[0], show=True)
# plt.savefig('lesson7/figures/waterfall_af_umap.png')

shap.plots.beeswarm(shap_values, show=True)
# plt.savefig('lesson7/figures/beeswarm_af_umap.png')

shap.plots.bar(shap_values, show=True)
# plt.savefig('lesson7/figures/bar_af_umap.png')

